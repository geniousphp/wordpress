FROM wordpress:4.9.4-php7.2-apache

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (print hash_file('SHA384', 'composer-setup.php') ===  file_get_contents('https://composer.github.io/installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

# install wordpress cli
# https://github.com/KaiHofstetter/docker-wordpress-cli/blob/master/Dockerfile
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp

RUN apt-get update; \
  apt-get install -y zip unzip libpng-dev

ENV NODE_VERSION 8.9.4
RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -SLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt" \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  && npm install -g webpack@3.10.0

ENV YARN_VERSION 1.3.2
RUN set -ex \
  && curl -fSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
  && mkdir -p /opt/yarn \
  && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/yarn --strip-components=1 \
  && ln -s /opt/yarn/bin/yarn /usr/local/bin/yarn \
  && ln -s /opt/yarn/bin/yarn /usr/local/bin/yarnpkg \
  && rm yarn-v$YARN_VERSION.tar.gz

ENTRYPOINT ["docker-php-entrypoint"]

ENV APP_PATH /var/www/wordpress
RUN mkdir -p $APP_PATH \
  mkdir -p $APP_PATH/web/app/themes/sage/dist \
  mkdir -p $APP_PATH/web/app/lara \
  && chown -R www-data:www-data /var/www && chmod g+s .


USER www-data

WORKDIR $APP_PATH/web/app/lara
ADD ./web/app/lara/composer.lock ./web/app/lara/composer.json ./
ADD web/app/lara/database ./database
RUN composer install --no-dev --no-scripts
ADD ./web/app/lara/yarn.lock ./web/app/lara/package.json ./
RUN yarn

WORKDIR $APP_PATH/web/app/themes/sage
ADD ./web/app/themes/sage/composer.lock ./web/app/themes/sage/composer.json ./
RUN composer install --no-dev
ADD ./web/app/themes/sage/yarn.lock ./web/app/themes/sage/package.json ./
RUN yarn

WORKDIR $APP_PATH
ADD ./composer.lock ./composer.json ./
RUN composer install --no-dev


WORKDIR $APP_PATH
COPY  --chown=www-data:www-data . $APP_PATH

WORKDIR $APP_PATH/web/app/themes/sage
RUN yarn run build:production

WORKDIR $APP_PATH/web/app/lara
RUN composer install --no-dev --optimize-autoloader \
  && touch .env \
  && php artisan key:generate \
  && npm run production


WORKDIR $APP_PATH

EXPOSE 8080
COPY apache-sites.conf /etc/apache2/sites-available/000-default.conf
COPY apache-ports.conf /etc/apache2/ports.conf


COPY wp_init.sh /opt/
USER root
RUN chmod +x /opt/wp_init.sh
USER www-data

# CMD ["apache2-foreground"]
CMD /opt/wp_init.sh && exec apache2-foreground